import React from 'react'
import ReactDOM from 'react-dom'

// Import non-js files like this, with the extension and an exclamation point:
import MyReactComponent from './MyReactComponent.jsx!'

(() => {
  ReactDOM.render(
    <MyReactComponent name="Daniel" />,
    document.getElementById('react-app')
  )
})()
