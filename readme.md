React project for playing and experimenting with various plugins etc.

* Uses node http-server for serving static files from public folder
  * Started by npm start
  * Site accessed at http://localhost:9020
* Web dependencies managed by jspm
  * Auto executed by script on npm install
  * npm script wrapper *npm run jspm <command>
